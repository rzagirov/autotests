# Данный тест запускается в случае фейла addndelete_from_contacts.py

import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import send_message

class DeleteUser_Doctor(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path='/Users/izeon123/drivers/chromedriver')

    def UserDelete(self):
        send_message.SendMessage.testSendMessage(self)
        driver = self.driver
        wait = WebDriverWait(driver, 10)
        wait.until(EC.visibility_of_element_located((By.ID, "toolbar_info_button")))
        toolbar_info = driver.find_element_by_id("toolbar_info_button")
        toolbar_info.click()
        wait.until(EC.visibility_of_element_located((By.ID, "activity_user_profile_more_button")))
        more_info = driver.find_element_by_id("activity_user_profile_more_button")
        more_info.click()
        wait.until(EC.visibility_of_element_located((By.ID, "activity_user_profile_more_remove")))
        delete_contact = driver.find_element_by_id("activity_user_profile_more_remove")
        delete_contact.click()

    def tear_down(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()

