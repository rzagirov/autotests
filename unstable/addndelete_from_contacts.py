import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import login
import time

class AddnDeleteFromContacts(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path='/Users/izeon123/drivers/chromedriver')

    def testAddnDeleteFromContacts(self):
        login.Login.testUserlogin(self)
        driver = self.driver
        wait = WebDriverWait(driver, 10)
        driver.get("https://app.dlg.im/#/im/discover")
        wait.until(EC.visibility_of_element_located((By.ID, "sidebar_header_plus_menu")))
        plus_button = driver.find_element_by_id("sidebar_header_plus_menu")
        plus_button.click()
        add_contact = driver.find_element_by_id("sidebar_header_plus_menu_add_contact")
        add_contact.click()
        wait.until(EC.visibility_of_element_located((By.ID, "add_contact_query")))
        add_contact_search_field = driver.find_element_by_id("add_contact_query")
        add_contact_search_field.send_keys("79117259840")
        time.sleep(2)
        wait.until(EC.visibility_of_element_located((By.ID, "add_contact_add_button")))
        add_contact_button = driver.find_element_by_id("add_contact_add_button")
        add_contact_button.click()
        time.sleep(3)
        wait.until(EC.visibility_of_element_located((By.ID, "toolbar_info_button")))
        info_button = driver.find_element_by_id("toolbar_info_button")
        info_button.click()
        wait.until(EC.visibility_of_element_located((By.ID, "activity_user_profile_more_button")))
        sub_menu_user = driver.find_element_by_id("activity_user_profile_more_button")
        sub_menu_user.click()
        wait.until(EC.visibility_of_element_located((By.ID, "activity_user_profile_more_remove")))
        remove_from_contacts_b = driver.find_element_by_id("activity_user_profile_more_remove")
        remove_from_contacts_b.click()
        wait.until(EC.visibility_of_element_located((By.ID, "confirm_success_button")))
        remove_from_cont_wind = driver.find_element_by_id("confirm_success_button")
        remove_from_cont_wind.click()
        wait.until(EC.visibility_of_element_located((By.PARTIAL_LINK_TEXT, "dialog!")))

    def tear_down(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()

