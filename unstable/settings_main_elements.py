import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
import login
import time


class UserAboutEdit(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path='/Users/izeon123/drivers/chromedriver')

    def test(self):
        driver = self.driver
        wait = WebDriverWait(driver, 10)
        login.Login.testUserlogin(self)
        dialog_icon = driver.find_element_by_id("sidebar_header_menu")
        dialog_icon.click()
        wait.until(EC.visibility_of_element_located((By.ID, "sidebar_header_menu_preferences")))
        preferences = driver.find_element_by_id("sidebar_header_menu_preferences")
        preferences.click()
        wait.until(EC.visibility_of_element_located((By.ID, "preferences_general_send_by_shift_enter")))
        sendby_shift_enter = driver.find_element_by_id("preferences_general_send_by_shift_enter")
        sendby_shift_enter.click()

    def tear_down(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
