import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import block_user

class UnBlockUser(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path='/Users/izeon123/drivers/chromedriver')

    def testUnBlockUser(self):
        block_user.BlockUser.testBlockUser(self)
        driver = self.driver
        wait = WebDriverWait(driver, 10)
        wait.until(EC.visibility_of_element_located((By.ID, "activity_user_profile_more_button")))
        more_info_unb = driver.find_element_by_id("activity_user_profile_more_button")
        more_info_unb.click()
        wait.until(EC.visibility_of_element_located((By.ID, "activity_user_profile_more_block")))
        unblock_contact = driver.find_element_by_id("activity_user_profile_more_block")
        unblock_contact.click()


    def tear_down(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()