import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import login

class BlockUser(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path='/Users/izeon123/drivers/chromedriver')

    def testBlockUser(self):
        login.Login.testUserlogin(self)
        driver = self.driver
        driver.get("https://app.dlg.im/#/im/discover")
        driver.get("https://app.dlg.im/#/im/u549726680") # Так нужно из-за либного бага, когда пофиксим тогда уберем
        wait = WebDriverWait(driver, 10)
        wait.until(EC.visibility_of_element_located((By.ID, "toolbar_info_button")))
        toolbar_info = driver.find_element_by_id("toolbar_info_button")
        toolbar_info.click()
        wait.until(EC.visibility_of_element_located((By.ID, "activity_user_profile_more_button")))
        more_info = driver.find_element_by_id("activity_user_profile_more_button")
        more_info.click()
        wait.until(EC.visibility_of_element_located((By.ID, "activity_user_profile_more_block")))
        block_contact = driver.find_element_by_id("activity_user_profile_more_block")
        block_contact.click()
        wait.until(EC.visibility_of_element_located((By.ID, "confirm_success_button")))
        confirm = driver.find_element_by_id("confirm_success_button")
        confirm.click()


    def tear_down(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()