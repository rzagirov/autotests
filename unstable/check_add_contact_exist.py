import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import login

class SendMessage(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path='/Users/izeon123/drivers/chromedriver')

    def testCheckAddContactExist(self):
        login.Login.testUserlogin(self)
        driver = self.driver
        wait = WebDriverWait(driver, 10)
        driver.get("https://app.dlg.im/#/im/discover")
        wait.until(EC.visibility_of_element_located((By.ID, "sidebar_header_plus_menu")))
        plus_button = driver.find_element_by_id("sidebar_header_plus_menu")
        plus_button.click()
        add_contact = driver.find_element_by_id("sidebar_header_plus_menu_add_contact")
        add_contact.click()
        wait.until(EC.visibility_of_element_located((By.ID, "add_contact_query")))
        add_contact_search_field = driver.find_element_by_id("add_contact_query")
        add_contact_search_field.send_keys("79117259840")
        wait.until(EC.visibility_of_element_located((By.ID, "add_contact_send_button")))
        send_message_check = driver.find_element_by_id("add_contact_send_button")
        send_message_check.click()
        wait.until(EC.visibility_of_element_located((By.ID, "chat_editor_textarea")))

    def tear_down(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()

