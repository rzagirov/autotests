import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import random
import string



class CreateChannel(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path='/Users/izeon123/drivers/chromedriver')

    def testCreateChannel(self):
        login.Login.testUserlogin(self)
        driver = self.driver
        wait = WebDriverWait(driver, 10)
        wait.until(EC.visibility_of_element_located((By.ID, "sidebar_header_plus_menu")))
        plus_button = driver.find_element_by_id("sidebar_header_plus_menu")
        plus_button.click()
        create_group_button = driver.find_element_by_id("sidebar_header_plus_menu_create")
        create_group_button.click()
        wait.until(EC.visibility_of_element_located((By.ID, "create_new_group_step_type_submit_button")))
        checkbox = driver.find_element_by_css_selector("label[for='create_new_group_type_channel']")
        checkbox.click()
        create_channel_window = driver.find_element_by_id("create_new_group_step_type_submit_button")
        create_channel_window.click()
        group_name = wait.until(EC.visibility_of_element_located((By.ID, "create_new_group_title")))
        group_name.send_keys(random.choices(string.ascii_uppercase + string.digits, k=6))
        group_link = wait.until(EC.visibility_of_element_located((By.ID, "create_new_group_shortname")))
        group_link.send_keys(random.choices(string.ascii_uppercase + string.digits, k=6))
        next_step = driver.find_element_by_id("create_new_group_step_info_submit_button")
        next_step.click()
        wait.until(EC.visibility_of_element_located((By.ID, "create_new_group")))
        find_contact = driver.find_element_by_class_name("dlg-dc350a130ae037b15b1c6fd4570fc309c7534fce")
        find_contact.send_keys("Radik")
        create_group_button_final = driver.find_element_by_id("create_new_group_finish_button")
        create_group_button_final.click()
        wait.until(EC.visibility_of_element_located((By.ID, "chat_editor_send_button")))

    def tear_down(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()