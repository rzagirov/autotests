import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import login
import time

class CallFromRecentTest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path='/Users/izeon123/drivers/chromedriver')

    def testCall(self):
        login.Login.testUserlogin(self)
        driver = self.driver
        wait = WebDriverWait(driver, 10)
        driver.get("https://app.dlg.im/#/im/discover")
        wait.until(EC.visibility_of_element_located((By.ID, "sidebar_footer_call_history_button")))
        call_history = driver.find_element_by_id("sidebar_footer_call_history_button")
        call_history.click()
        time.sleep(5)
        wait.until(EC.visibility_of_element_located((By.ID, "sidebar_call_item_-4292864603343968242")))
        call_recent = driver.find_element_by_id("sidebar_call_item_-4292864603343968242")
        call_recent.click()
        wait.until(EC.visibility_of_element_located((By.CLASS_NAME, "DraftEditor-editorContainer")))
        time.sleep(3)
        call = driver.find_element_by_id("toolbar_call_button")
        call.click()
        time.sleep(10)


    def tear_down(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()