import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import login

class SendMessage(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path='/Users/izeon123/drivers/chromedriver')

    def testSendMessage(self) -> object:
        login.Login.testUserlogin(self)
        driver = self.driver
        message_icon = driver.find_element_by_class_name("dlg-7923707f1bcfd461b622a172a2646c3ac6cbb037")
        message_icon.click()
        driver.get("https://app.dlg.im/#/im/u549726680")
        wait = WebDriverWait(driver, 10)
        wait.until(EC.visibility_of_element_located((By.CLASS_NAME, "DraftEditor-editorContainer")))
        enter_text = driver.find_element_by_xpath("//*[@class='notranslate public-DraftEditor-content']")
        enter_text.click()
        enter_text.send_keys("Test_message")
        click_send = driver.find_element_by_id("chat_editor_send_button")
        click_send.click()



    def tear_down(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()

