import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import login

class SendMessage(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path='/Users/izeon123/drivers/chromedriver')

    def testSendMessage(self):
        login.Login.testUserlogin(self)
        driver = self.driver
        driver.get("https://app.dlg.im/#/im/u10")
        wait = WebDriverWait(driver, 10)
        wait.until(EC.visibility_of_element_located((By.ID, "chat_editor_textarea")))
        smile_button = driver.find_element_by_id("chat_editor_emoji_button")
        smile_button.click()
        smile_choose = driver.find_element_by_xpath("//*[contains(@title, 'grinning')]")
        smile_choose.click()
        send_message_button = driver.find_element_by_id("chat_editor_send_button")
        send_message_button.click()

    def tear_down(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()

