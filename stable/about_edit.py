import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
import login
import time


class UserAboutEdit(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path='/Users/izeon123/drivers/chromedriver')

    def test(self):
        driver = self.driver
        wait = WebDriverWait(driver, 10)
        login.Login.testUserlogin(self)
        dialog_icon = driver.find_element_by_id("sidebar_header_menu")
        dialog_icon.click()
        wait.until(EC.visibility_of_element_located((By.ID, "sidebar_header_menu_edit_profile")))
        edit_profile = driver.find_element_by_id("sidebar_header_menu_edit_profile")
        edit_profile.click()
        wait.until(EC.visibility_of_element_located((By.ID, "profile_modal_about")))
        about_field = driver.find_element_by_id("profile_modal_about")
        about_field.send_keys("autotest")
        submit_button_1 = driver.find_element_by_id("profile_modal_submit_button")
        submit_button_1.click()
        time.sleep(5)
        wait.until(EC.visibility_of_element_located((By.ID, "sidebar_header_menu")))
        dialog_icon_2 = driver.find_element_by_id("sidebar_header_menu")
        dialog_icon_2.click()
        edit_profile_2 = driver.find_element_by_id("sidebar_header_menu_edit_profile")
        edit_profile_2.click()
        wait.until(EC.visibility_of_element_located((By.ID, "profile_modal_about")))
        about_field_2 = driver.find_element_by_id("profile_modal_about")
        about_field_2.clear()
        submit_button_2 = driver.find_element_by_id("profile_modal_submit_button")
        submit_button_2.click()

    def tear_down(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
