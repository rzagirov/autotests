import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import login
import time

class SearchBar(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path='/Users/izeon123/drivers/chromedriver')

    def testSearchBar(self):
        login.Login.testUserlogin(self)
        driver = self.driver
        message_icon = driver.find_element_by_id("sidebar_footer_recent_button")
        message_icon.click()
        wait = WebDriverWait(driver, 10)
        time.sleep(3)
        wait.until(EC.visibility_of_element_located((By.ID, "sidebar_search_input")))
        click_search_messages = driver.find_element_by_id("sidebar_search_input")
        click_search_messages.send_keys("Radik")
        wait.until(EC.visibility_of_element_located((By.XPATH, "//*[contains(@id, 'sidebar_peer_item_')]")))
        contact_click = driver.find_element_by_xpath("//*[contains(@id, 'sidebar_peer_item_')]")
        contact_click.click()

    def tear_down(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()

