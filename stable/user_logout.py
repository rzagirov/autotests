import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import login


class SendMessage(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path='/Users/izeon123/drivers/chromedriver')

    def testUserlogin(self):
        login.Login.testUserlogin(self)
        driver = self.driver
        wait = WebDriverWait(driver, 10)
        dialog_icon = driver.find_element_by_id("sidebar_header_menu")
        dialog_icon.click()
        wait.until(EC.visibility_of_element_located((By.ID, "sidebar_header_menu_logout")))
        edit_profile = driver.find_element_by_id("sidebar_header_menu_logout")
        edit_profile.click()
        wait.until(EC.visibility_of_element_located((By.ID, "confirm_success_button")))
        logout_button = driver.find_element_by_id("confirm_success_button")
        logout_button.click()
        wait.until(EC.visibility_of_element_located((By.ID, "form_login_login")))

    def tear_down(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()