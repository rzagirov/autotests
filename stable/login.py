import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class Login(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path='/Users/radik/drivers/chromedriver')

    def testUserlogin(self):
        driver = self.driver
        wait = WebDriverWait(driver, 10)
        driver.get("https://app.dlg.im/#/auth")
        wait.until(EC.visibility_of_element_located((By.ID, "form_login_login")))
        login_field = driver.find_element_by_id("form_login_login")
        login_field.clear()
        login_field.send_keys("755525555555")
        submit_field = driver.find_element_by_class_name("dlg-2861b42e5eeef617d526528b1c805ecad85f1240")
        submit_field.click()
        wait.until(EC.visibility_of_element_located((By.ID, "form_login_code")))
        code_field = driver.find_element_by_id("form_login_code")
        code_field.click()
        code_field.send_keys("22222")
        wait.until(EC.visibility_of_element_located((By.CLASS_NAME, "dlg-7923707f1bcfd461b622a172a2646c3ac6cbb037")))

    def tear_down(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
