import time
import unittest
import create_group_private
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


class CallTest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path='/Users/izeon123/drivers/chromedriver')
    def testCall(self):
        create_group_private.CreateGroup.testCreateGroup(self)
        driver = self.driver
        wait = WebDriverWait(driver, 10)
        wait.until(EC.visibility_of_element_located((By.CLASS_NAME, "DraftEditor-editorContainer")))
        time.sleep(3)
        call = driver.find_element_by_id("toolbar_call_button")
        call.click()
        time.sleep(10)


    def tear_down(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()