import unittest
from appium import webdriver


class AppTestAppium(unittest.TestCase):
    def setUp(self):
        caps = {}
        caps['browserName'] = ""
        caps['udid'] = "GCAXGY09U611ZRM"
        caps['appiumVersion'] = "1.7.1"
        caps['deviceName'] = "Android"
        #caps['app'] = ""
        caps['platformVersion'] = "7.0"
        caps['platformName'] = "Android"
        caps['appPackage'] = "im.dlg.app"
        caps['appActivity'] = 'im.dlg.sdk.MainActivity'
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', caps)

    def test_ClickRefreshLink(self):
        driver = self
        refreshButton = self.driver.find_element_by_id("com.witmergers.getstatus:id/fab")
        self.assertTrue(refreshButton.is_displayed())
        refreshButton.click()

    def tearDown(self):
        self.driver.quit()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(AppTestAppium)
    unittest.TextTestRunner(verbosity=2).run(suite)
