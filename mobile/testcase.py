import os
import unittest
from appium import webdriver
from time import sleep


class AppTestAppium(unittest.TestCase):
    def setUp(self):
        username = 'rzagirov'
        password = 'a31134df-67a0-48d1-bd3a-b70a09822c5a'
        caps = {}
        caps['browserName'] = "Browser"
        caps['appiumVersion'] = "1.7.1"
        caps['deviceName'] = "Android Emulator"
        caps['deviceOrientation'] = "portrait"
        caps['platformVersion'] = "6.0"
        caps['platformName'] = "Android"
        caps['appPackage'] = "im.dlg.app"
        caps['appActivity'] = 'im.dlg.sdk.MainActivity'
        self.driver = webdriver.Remote

    def tearDown(self):
        self.driver.quit()

    def test_ClickRefreshLink(self):
        driver = self.driver
        refreshButton = driver.find_element_by_class_name("android.widget.Button")
        refreshButton.click()




if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(AppTestAppium)
    unittest.TextTestRunner(verbosity=2).run(suite)
